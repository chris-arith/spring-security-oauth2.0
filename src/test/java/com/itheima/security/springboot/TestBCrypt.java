package com.itheima.security.springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 董文登
 * @date 2020/3/2
 */
@RunWith(SpringRunner.class)
public class TestBCrypt {

    @Test
    public void testBCrypt() {

        // 对密码进行加密
        String hashpw = BCrypt.hashpw("123", BCrypt.gensalt());
        System.out.println(hashpw);

        // 校验密码
        String hashed1 = "$2a$10$XM8NBcgvBRiOwogXcko.8uZPfuU5doammZ.ILYUs.TgIaSvwQgt6W";
        String hashed2 = "$2a$10$L/YEEo5tFbBXmzLpkCmtL.IQ3Yxazaw2G4WsJdpWtuhHVJ0JTfhpu";
        boolean checkpw1 = BCrypt.checkpw("123", hashed1);
        boolean checkpw2 = BCrypt.checkpw("123", hashed2);
        System.out.println(checkpw1);
        System.out.println(checkpw2);
    }
}
