package com.ithiema.security.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * @author 董文登
 * @date 2020/2/29
 * @description 这个类就是servletContext
 */

/* 注意springboot项目和我们普通的maven项目不同，由于springboot自动装配的特性，EnableWebMVC注解可以省略；
* 由于springboot启动类在项目的根目录下，所以通过启动类启动项目时，springboot会自动扫描启动类所在包下的所有包，所以扫描包注解也不需要，
* 只需要@configuration注解标注这是一个配置类即可 */
//@EnableWebMvc
//@ComponentScan(basePackages = "com.itheima.security.springmvc"
//        , includeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = Controller.class)})

@Configuration
public class WebConfig implements WebMvcConfigurer {

    // springsecurity认证，授权，拦截的功能，所以我们无需自己配置拦截器
//    @Autowired
//    SimpleAuthenticationInterceptor simpleAuthenticationInterceptor;

    // 视图解析器也许需要了，只需要在springboot配置文件中配置一下前后缀即可
//    @Bean
//    public InternalResourceViewResolver viewResolver() {
//        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//        viewResolver.setPrefix("/WEB-INF/view/");
//        viewResolver.setSuffix(".jsp");
//        return viewResolver;
//    }

    // 增加视图控制器
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // /(根目录)重定向到/login-view
        registry.addViewController("/").setViewName("redirect:/login-view");
        // login-view对应的视图是login，直接重定向到login会重定向到security自带的登录页面
        registry.addViewController("/login-view").setViewName("login");
    }

//    // 增加拦截器
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        // 将我们实现的拦截器添加进来
//        registry.addInterceptor(simpleAuthenticationInterceptor).addPathPatterns("/r/**");
//    }
}
