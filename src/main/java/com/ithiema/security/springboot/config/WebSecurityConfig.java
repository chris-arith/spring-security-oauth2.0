package com.ithiema.security.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * @author 董文登
 * @date 2020/3/1
 * 继承WebSecurityConfigurerAdapter这个类，告知security框架，这是类是用来配置用户信息、密码编码器、安全拦截机制等功能的
 * @EnableWebSecurity 该注解表示开启security功能
 */

//@EnableWebSecurity springboot的自动装配，这个注解也不需要了

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    // 在我们自定义的userDetailsService中定义，这里可以屏蔽掉了
//    // 定义用户信息服务（查询用户信息）
//    @Bean
//    public UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withUsername("zhangsan").password("123").authorities("p1").build());
//        manager.createUser(User.withUsername("lisi").password("456").authorities("p2").build());
//        return manager;
//    }

    // 密码编码器
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        // NoOpPasswordEncoder表示直接明文比较，不做加密
//        return NoOpPasswordEncoder.getInstance();
//    }
    // 采用BCryptPasswordEncoder加密方式
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // 定义安全拦截机制--最重要

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 需要特别注意的是，如果想要通过get请求访问/logout退出登录，必须关闭csrf防跨域功能！！！
        http.csrf().disable()// 放开限制跨域功能
                .authorizeRequests()
                // 访问指定资源需要什么样的授权
                .antMatchers("/r/r1").hasAuthority("p1")
                .antMatchers("/r/r2").hasAuthority("/p2")
                // 表示所有/r/**的请求，必须认证通过
                .antMatchers("/r/**").authenticated()
                // 表示出了/r/** 的请求，其他的请求都可以通过
                .anyRequest().permitAll()
                .and()
                // 表示允许表单登录
                .formLogin()
                // 登录页面
                .loginPage("/login-view")
                // 登录页面中的要跳转的url
                .loginProcessingUrl("/login")
                // 自定义登录成功后跳转的页面地址
                .successForwardUrl("/login-success")
            .and()
                .sessionManagement()
                // 配置session创建策略为如果需要就创建session
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
            .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login-view?logout");
    }
}
